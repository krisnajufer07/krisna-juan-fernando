function getBiodata(event) {
    event.preventDefault();
    let fullname = $(`#firstname`).val() + ` ` + $(`#lastname`).val();
    let phone = $(`#phone_number`).val();
    let bio = $(`#bio`).val();
    let gender = $(`#gender`).val();
    if ($('#remember').is(':checked')) {
        if (fullname !== "" && phone !== "" && bio !== "" && gender !== "") {
            $(`#fullname`).val(fullname);
            $(`#number`).val(phone);
            $(`#show-bio`).val(bio);
            $(`#show-gender`).val(gender);
        } else {
            alert(`field cannot be empty`);
        }
    } else {
        alert(`please check the agreement`);
    }
}

function InputNumbers(evt) {
    var key = String.fromCharCode(evt.which);
    if (!(/[0-9]/.test(key))) {
        evt.preventDefault();
    }
};